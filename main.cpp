#include "iostream"
#include "string"

using namespace std;

// A 65
// Z 90
// a 97
// z 122

string cipher(string m, string k)
{	
	int x = 0;
	
	for(int i = 0; i < m.length(); i++)
	{
		//cout << "i = " << i << ", " << m[i + x] << ", " << k[(i + x) % k.length()] << "\n";
		if(m[i] != 32)
			m[i] = (((m[i] - 65) + (k[(i - x) % k.length()] - 65)) % 26) + 65;
		else
			x++;
	}
		
	return m;
}

string decipher(string s, string k)
{
	string x = k;
	
	for(int i = 0; i < k.length(); i++)
	{
		x[i] = ((26 - (k[i] - 65)) % 26) + 65;
	}
	
	//cout << x << "\n";
	
	return cipher(s, x);
}

string toUpper(string s)
{
	for(int i = 0; i < s.length(); i++)
	{
		if(s[i] == 32 && (s[i] > 64 && s[i] < 91))
			;
		else
		{
			if(s[i] > 96 && s[i] < 123)
				s[i] = s[i] - 32;
		}
	}
	
	return s;
}

int main()
{
	string m;
	string k;
	
	cout << "Prezentacja algorytmu Vigenere'a\nProsze wpisac informacje do zaszyfrowania:\n";
	getline(cin, m);
	m = toUpper(m);
	
	cout << "Prosze wpisac klucz:\n";
	cin >> k;
	k = toUpper(k);
	
	cout << "Zaszyfrowana informacja:\n";
	cout << cipher(m, k);
	
	cout << "\nOdszyfrowana informacja:\n";
	cout << decipher(cipher(m, k), k);
	cout << "\nWcisnij dowolny przycisk, aby zakonczyc dzialanie programu\n";
	
	cin.get();
	cin.get();
	
	return 0;
}
